import React from 'react';
import { BrowserRouter as Router,Route} from 'react-router-dom';
import Upload from './components/container/Upload';

import './App.css';	

const App = (props) => (
    <Router>
    	<div className="App">
    	<Route path="/upload" component={Upload}/>
    	</div>
    </Router>
)

export default App;
